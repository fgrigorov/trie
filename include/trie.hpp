#pragma once
#include <string>
#include <vector>

struct Node;

class Trie
{
public:
	Trie();
	~Trie() { if (!is_empty_) Empty(); }

	void Empty();
	void Erase(const std::string& word);

	bool Insert(const std::string& word);

	std::vector<std::pair<std::string, bool>> Search(const std::string& prefix);

private:
	bool is_empty_ = true;

	Node* root_ = nullptr;
};
