#include <algorithm>
#include <iostream>

#include "trie.hpp"

int main()
{
	std::vector<std::string> words = {
		"dog",
		"cat",
		"doggy",
		"dogder",
		"canada",
		"do"
	};

	Trie trie;
	for (const auto& word : words)
	{
		trie.Insert(word);
	}

	std::vector<std::string> test_words = {
		"dog"
	};

	for (const auto& test_word : test_words)
	{
		auto results = trie.Search(test_word);

		std::for_each(std::cbegin(results), std::cend(results), [&test_word](const auto& result) {
			std::cout << "Found \"" << result.first << "\" for " << test_word << "!" << std::endl;
		});
	}

	//trie.Empty();
	trie.Erase("doggy");

	return EXIT_SUCCESS;
}