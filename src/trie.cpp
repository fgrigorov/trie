#include <algorithm>
#include <cassert>
#include <iostream>
#include <stdexcept>

#include "trie.hpp"

//TODO: Add isLeaf (after a word), so we do not miss it if it is root to another
/*
	Memory footprint:
	node_size = sizeof(char) + sizeof(std::vector<Node*>.size()) + sizoef(struct Node)
	memory = node_size + SUM_i(len_word) / n_chars bytes

	Time complexity:

	m: number of children
	n: number of characters per word (number of nodes)

	(i) insert: O(n * m) + O(n)
	(ii) search: O(n * m)
	(iii) erase: O(n * m) + O(n)
	(iv) empty: O(1) + O(1)
*/

struct Node
{
	Node(char value) : value_(value) {}

	char value_ = '0';
	std::vector<Node*> nodes_;
};

namespace
{
	void erase_from_node(Node* node)
	{
		node->nodes_.clear();
	}

	void count_intersections(Node* node, const std::string& str, std::vector<std::pair<int, Node*>>& visited_nodes)
	{
		if (node->nodes_.empty())
			return;

		for (auto idx = 0; idx < node->nodes_.size(); ++idx)
		{
			if (node->nodes_[idx]->value_ == str.front()) {
				visited_nodes.emplace_back(static_cast<int>(node->nodes_[idx]->nodes_.size()), node->nodes_[idx]);

				count_intersections(node->nodes_[idx], std::string(str.cbegin() + 1, str.cend()), visited_nodes);
			}
		}
	}

	bool is_visited(Node* node, std::vector<Node*>& visited_nodes)
	{
		for (auto idx = 0; idx < visited_nodes.size(); ++idx)
		{
			if (visited_nodes[idx] == node)
				return true;
		}

		return false;
	}

	void fetch_descendents_from_node(Node* node, std::vector<std::pair<std::string, bool>>& matches, std::vector<Node*>& visited_nodes)
	{
		if (node->nodes_.empty())
			return;

		//What we have, we create copies: new = old + new_size - 1
		for (auto idx = 0; idx < node->nodes_.size() - 1; ++idx)
			matches.push_back(matches.back());

		for (auto i = 0; i < node->nodes_.size(); ++i)
		{
			for (auto ii = i; ii < matches.size(); ++ii)
			{
				Node* child = node->nodes_[i];
				if (!matches[ii].second && !is_visited(child, visited_nodes))
				{
					matches[ii].first += child->value_;
					if (child->nodes_.empty()) {
						matches[ii].second = true;
						visited_nodes.push_back(node);
						break;
					}
					fetch_descendents_from_node(child, matches, visited_nodes);
				}
			}
		}
	}

	bool insert_str_to_node_tree(Node* node, const std::string& str)
	{
		if (str.empty())
			return true;

		node->nodes_.push_back(new Node(str.front()));
		return insert_str_to_node_tree(node->nodes_.back(), std::string(str.cbegin() + 1, str.cend()));
	}

	Node* locate_node_from_prefix(Node* node, const std::string& prefix) {
		if (prefix.empty())
			return node;

		Node* found_node = nullptr;
		for (auto idx = 0; idx < node->nodes_.size(); ++idx)
		{
			if (node->nodes_[idx]->value_ == prefix.front())
			{
				found_node = node->nodes_[idx];
				break;
			}
		}
		if (found_node)
			return locate_node_from_prefix(found_node, std::string(prefix.cbegin() + 1, prefix.cend()));

		return found_node;
	}
}  // namespace

Trie::Trie()
{
	root_ = new Node('0');
	is_empty_ = false;
}

void Trie::Empty()
{
	if (!root_->nodes_.empty())
		root_->nodes_.clear();
	if (root_) {
		delete root_;
		is_empty_ = true;
	}
}

void Trie::Erase(const std::string& word)
{
	//Start going through nodes until children are more than 2
	//If not, delete the whole path (keep track of it) else start deleting from where children are more than 1
	std::vector<std::pair<int, Node*>> visited_nodes;
	count_intersections(root_, word, visited_nodes);

	std::pair<int, Node*> last_found_pair = std::make_pair(-1, nullptr);
	for (auto idx = 0; idx < visited_nodes.size(); ++idx)
	{
		if (visited_nodes[idx].first > 1)
		{
			last_found_pair = visited_nodes[idx];
		}
	}

	if (last_found_pair.first > 1)
	{
		erase_from_node(last_found_pair.second);
	}
}

bool Trie::Insert(const std::string& word)
{
	if (word.empty())
		throw std::runtime_error("Could not import empty string");

	//from_node will include the last encountered character
	Node* from_node = nullptr;
	auto is_stop = false;
	std::string str = word;
	do
	{
		from_node = locate_node_from_prefix(root_, str);
		if (!str.empty())
		{
			if (!from_node)
			{
				str = std::string(str.cbegin(), str.cend() - 1);
			}
			else if (from_node && str.size() == word.size())
			{
				return false;
			}
			else if (from_node && str.size() < word.size())
			{
				auto found_at_idx = word.find(from_node->value_);
				str = word.substr(found_at_idx + 1);
				is_stop = true;
			}
		}
		else 
		{
			from_node = root_;
			str = word;
			is_stop = true;
		}
	} while (!is_stop);

	return insert_str_to_node_tree(from_node, str);
}

std::vector<std::pair<std::string, bool>> Trie::Search(const std::string& prefix)
{
	std::vector<std::pair<std::string, bool>> results = { std::make_pair(prefix, false) };
	auto from_node = locate_node_from_prefix(root_, prefix);

	if (!from_node || from_node->nodes_.empty())
		return results;
	
	std::vector<Node*> visited_nodes;
	fetch_descendents_from_node(from_node, results, visited_nodes);

	return results;
}
